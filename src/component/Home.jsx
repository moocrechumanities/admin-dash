import React, { Component } from 'react'
import $ from 'jquery'
import axios from 'axios';
import { Button } from 'semantic-ui-react'


export default class Home extends Component {

        constructor(props){
                super(props)
                this.state = {
                        loading : 'loading',
                        importStatus : {},
                        statusCheck:{},
                        importId:'',
                        url:''
                }
        }
        componentDidMount(){
                let _this = this;

                $('#b').on('click',function(){
                _this.setState({store : Math.random()});
                })

        }

        onSubmitPage =  (event)=>{
                event.stopPropagation(); 
                event.preventDefault(); 

                this.ImportStatus(this.state.url, this.state.importId).then(function(){

                });

        }        

        onIportId = (event)=>{
                this.email = event.target.value;
                this.setState({email : this.email});
        }

        onUrl = (event)=>{
                this.email = event.target.value;
                this.setState({email : this.email});
        }
        
        
render() {

        return (
        <div>
                <center>

                        <br/>
                        <br/><br/>

                        <h2>MOOC DASHBOARD      </h2>
                        <form action="" method="POST" onSubmit={(event)=>this.onSubmitPage(event)}>

                        <div class="form-group">
                                <input  class = "col-md-2"
                                        type="text" 
                                        className="form-control" 
                                        id="importId" 
                                        placeholder="IMPORT-ID"
                                        onChange ={(event)=>this.onIportId(event)}
                                />
                                <br/>
                                <input 
                                        type="text" 
                                        className="form-control" 
                                        id="url" 
                                        placeholder="URL"
                                        onChange ={(event)=>this.onIportId(event)}
                                />
                                <br/>
                                <Button type ="submit">
                                        Loading
                                </Button>
                        </div>

                        </form>
                </center>
        </div>
        )
        }


        ImportStatus = (url,id) =>{

                return new Promise ((resolve,reject)=>{
                
                        // url given by input as = https://mooc-connector-storage.herokuapp.com//mooc-humanities-connector-storage 
                        axios.get(url+'/start-job?import-id='+ id ).then(function(res){
                        
                        let status = res.data;
                        this.setState({status : status});
                        resolve();  

                        }.bind(this)).catch(function (error) {
                                console.log(error);
                                reject();
                        });

                })
        }

        // to check the progress
        getImportStatus = (id) =>{

                return new Promise ((resolve,reject)=>{
                
                        axios.get('https://mooc-connector-storage.herokuapp.com//mooc-humanities-connector-storage/status?import-id='+ id ).then(function(res){
                        
                        let status = res.data;
                        this.setState({ statusCheck : status});
                        resolve();  

                        }.bind(this)).catch(function (error) {
                                console.log(error);
                                reject();
                        });

                })
        }
        

}
