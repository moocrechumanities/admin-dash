import React, { Component } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import Route from 'react-router-dom/Route';
import Home from './component/Home'


class App extends Component {
  render() {
    return (
      <Router>
      <div >
          <Switch>
                  <Route path="/" exact static  component={Home} />
          </Switch>
      </div>
  </Router>

    );
  }
}

export default App;
